#pragma semicolon 1
#pragma newdecls required
#include <sourcemod>
#include <highlander_shuffle_matchmaker>

/*
 * plugin info
 */
public Plugin myinfo = {
	name = "Highlander Shuffle",
	author = "smiley & koi",
	description = "",
	version = "0.0.1",
	url = "http://www.google.com"
};

/*
 * plugin defines
 */
const int MAX_CLIENTS = 33;

/*
 * per player data
 */
bool playerConnected[ MAX_CLIENTS ];
bool playerAuthorized[ MAX_CLIENTS ];
char playerSteamID[ MAX_CLIENTS ][ 256 ];
int playerMMID[ MAX_CLIENTS ];

/*
 * handle client request to check matchmaker status
 */
static void Status( int client ) {
	if ( playerConnected[ client ] == false ) {
		ReplyToCommand( client, "client %d not connected - cannot check status", client );
		return;
	}

	if ( playerAuthorized[ client ] == false ) {
		ReplyToCommand( client, "client %d not authorized - cannot check status", client );
		return;
	}

	ReplyToCommand( client, "you are %s - there are %d players in the queue",
		playerMMID[ client ] != 0 ? "queued" : "not queued",
		HSMM_GetNumRegisteredClients() );
	return;
}

/*
 * handle client request to queue
 * only attempts to register connected and authorized clients
 */
static void Queue( int client ) {
	if ( playerConnected[ client ] == false ) {
		ReplyToCommand( client, "client %d not connected - cannot queue", client );
		return;
	}

	if ( playerAuthorized[ client ] == false ) {
		ReplyToCommand( client, "client %d not authorized - cannot queue", client );
		return;
	}

	if ( playerMMID[ client ] != 0 ) {
		ReplyToCommand( client, "client %d already registered to matchmaker - cannot queue",
			client );
		return;
	}

	playerMMID[ client ] = HSMM_RegisterClient( playerSteamID[ client ] );
	if ( playerMMID[ client ] == 0 ) {
		ReplyToCommand( client, "unable to register client %d with id %s - cannot queue",
			client, playerSteamID[ client ] );
		return;
	}

	ReplyToCommand( client, "registered client %d with id %s to matchmaker - mmid %d",
		client, playerSteamID[ client ], playerMMID[ client ] );
	PrintToServer( "[highlander_shuffle] client %d with id %s is now registered with the matchmaker - mmid %d",
		client, playerSteamID[ client ], playerMMID[ client ] );
	return;
}

/*
 * handle client request to dequeue
 * only attempts to deregister registered clients
 */
static void Dequeue( int client ) {
	if ( playerConnected[ client ] == false ) {
		ReplyToCommand( client, "client %d not connected - cannot dequeue", client );
		return;
	}

	if ( playerAuthorized[ client ] == false ) {
		ReplyToCommand( client, "client %d not authorized - cannot dequeue", client );
		return;
	}

	if ( playerMMID[ client ] == 0 ) {
		ReplyToCommand( client, "client %d not registered - cannot dequeue", client );
		return;
	}

	HSMM_UnregisterClient( playerMMID[ client ] );
	playerMMID[ client ] = 0;

	ReplyToCommand( client, "unregisterd client %d with id %s from matchmaker",
		client, playerSteamID[ client ] );
	PrintToServer( "[highlander_shuffle] client %d with id %s is now unregistered from the matchmaker",
		client, playerSteamID[ client ] );
	return;
}

/*
 * handle client menu actions
 */
public int MenuHandler( Menu menu, MenuAction action, int param1, int param2 ) {
	if ( action == MenuAction_Select ) {
		char info[ 32 ];
		bool found = menu.GetItem( param2, info, sizeof( info ) );
		if ( found ) {
			if ( strcmp( info, "status" ) == 0 ) {
				Status( param1 );
			} else if ( strcmp( info, "queue" ) == 0 ) {
				Queue( param1 );
			} else if ( strcmp( info, "dequeue" ) == 0 ) {
				Dequeue( param1 );
			}
		}
	} else if ( action == MenuAction_Cancel ) {
		// nothing to do
	} else if ( action == MenuAction_End ) {
		delete menu;
	}
}

public Action ConsoleCmd_Menu( int client, int args ) {
	if ( playerConnected[ client ] == false ) {
		ReplyToCommand( client, "client %d not connected - cannot open menu", client );
		return Plugin_Handled;
	}

	if ( playerAuthorized[ client ] == false ) {
		ReplyToCommand( client, "client %d not authorized - cannot open menu", client );
		return Plugin_Handled;
	}

	Menu menu = new Menu( MenuHandler );
	menu.SetTitle( "Highlander Shuffle" );
	menu.AddItem( "status", "Check Status" );
	if ( playerMMID[ client ] == 0 ) {
		menu.AddItem( "queue", "Join The Queue" );
	} else {
		menu.AddItem( "dequeue", "Leave The Queue" );
	}
	menu.Display( client, 10 );

	return Plugin_Handled;
}

public Action ConsoleCmd_Status( int client, int args ) {
	Status( client );
	return Plugin_Handled;
}

public Action ConsoleCmd_Queue( int client, int args ) {
	Queue( client );
	return Plugin_Handled;
}

public Action ConsoleCmd_Dequeue( int client, int args ) {
	Dequeue( client );
	return Plugin_Handled;
}

public void OnPluginStart() {
	RegConsoleCmd( "hsmenu", ConsoleCmd_Menu );
	RegConsoleCmd( "hsstatus", ConsoleCmd_Status );
	RegConsoleCmd( "hsqueue", ConsoleCmd_Queue );
	RegConsoleCmd( "hsdequeue", ConsoleCmd_Dequeue );
}

public void OnClientConnected( int client ) {
	// initialize player data
	playerConnected[ client ] = true;
	playerAuthorized[ client ] = false;
	strcopy( playerSteamID[ client ], 256, "" );
	playerMMID[ client ] = 0;

	PrintToServer( "[highlander_shuffle] client %d connected", client );
}

public void OnClientAuthorized( int client, const char[] id ) {
	playerAuthorized[ client ] = true;
	strcopy( playerSteamID[ client ], 256, id );

	PrintToServer( "[highlander_shuffle] client %d authorized %s", client, id );
}

public void OnClientDisconnect( int client ) {
	if ( playerMMID[ client ] != 0 ) {
		HSMM_UnregisterClient( playerMMID[ client ] );
		playerMMID[ client ] = 0;
		PrintToServer( "[highlander_shuffle] client %d is now unregistered from the matchmaker", client );
	}

	playerConnected[ client ] = false;

	PrintToServer( "[highlander_shuffle] client %d disconnected", client );
}

public void OnGameFrame() {
	for ( int i = 0; i < MAX_CLIENTS; ++i ) {
		if ( playerConnected[ i ] == false ) {
			continue;
		}
		if ( playerAuthorized[ i ] == false ) {
			continue;
		}
		// connected and authorized client
	}
	HSMM_Tick();
}
