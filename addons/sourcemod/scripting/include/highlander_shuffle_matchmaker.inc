#if defined _highlander_shuffle_matchmaker_included
	#endinput
#endif
#define _highlander_shuffle_matchmaker_included

int nextMMID = 1;
int numRegisteredClients = 0;

/*
 * registers a client given the client's steam id
 * returns matchmaker id ( MMID )
 */
stock int HSMM_RegisterClient( const char[] id ) {
	int MMID = nextMMID;
	nextMMID += 1;
	numRegisteredClients += 1;
	return MMID;
}

/*
 * unregisters a client given the client's MMID
 */
stock void HSMM_UnregisterClient( int MMID ) {
	numRegisteredClients -= 1;
}

/*
 * returns the number of clients currently registered
 */
stock int HSMM_GetNumRegisteredClients() {
	return numRegisteredClients;
}

/*
 * call once per frame
 */
stock void HSMM_Tick() {
	//
}
